  <!--footer starts here-->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-xl-5 offset-xl-1">
          <div class="footer-left">
            <?php the_field('footer_left_top', 'option'); ?>
            <div class="sm">
              <a href="<?php the_field('olin_facebook', 'option'); ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/icon-facebook.png" alt=""></a>
              <a href="<?php the_field('olin_twitter', 'option'); ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/icon-twitter.png" alt=""></a>
              <a href="<?php the_field('olin_flickr', 'option'); ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/icon-flickr.png" alt=""></a>
              <a href="<?php the_field('olin_youtube', 'option'); ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/icon-youtube.png" alt=""></a>
              <a href="<?php the_field('olin_linkedin', 'option'); ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/icon-linkedin.png" alt=""></a>
              <a href="<?php the_field('olin_instagram', 'option'); ?>" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/icon-instagram.png" alt=""></a>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-xl-5">
          <div class="footer-right">
            <ul>

              <li>
                <a href="https://www.olin.edu/invest/ways/making-your-gift-olin-college/" class="" target="_blank">Give to Olin</a>
              </li>

              <li>
                <a href="https://www.olin.edu/news-events/" class="" target="_blank">News &amp; Events</a>
              </li>

              <li>
                <a href="http://olin.edu/academic-life/student-affairs-resources/registrar/" class="" target="_blank">Registrar</a>
              </li>

              <li>
                <a href="http://olin.edu/campus/" class="" target="_blank">Campus</a>
              </li>

              <li>
                <a href="http://olin.edu/join-community/" class="" target="_blank">Careers</a>
              </li>

              <li>
                <a href="http://olin.edu/admission/" class="" target="_blank">Admission</a>
              </li>

              <li>
                <a href="http://olin.edu/academic-life/student-affairs-and-resources/sexual-misconduct-info/" class="" target="_blank">Sexual Misconduct &amp; Title IX</a>
              </li>
            </ul>
            <ul>

              <li>
                <a href="http://olin.edu/faculty-staff-directory/" class="" target="_blank">Fac/Staff Directory</a>
              </li>

              <li>
                <a href="http://olin.edu/collaborate/" class="" target="_blank">Collaborate with olin</a>
              </li>

              <li>
                <a href="https://www.olin.edu/contact-us/" class="" target="_blank">Contact</a>
              </li>

              <li>
                <a href="http://olin.edu/directions/" class="" target="_blank">Directions</a>
              </li>

              <li>
                <a href="http://olin.edu/community/join/equal-opportunity/" class="" target="_blank">Equal opportunity</a>
              </li>

              <li>
                <a href="http://olin.edu/privacy-policy/" class="" target="_blank">privacy/legal</a>
              </li>

              <li>
                <a href="http://olin.edu/sitemap/" class="" target="_blank">sitemap</a>
              </li>

              <li>
                <a href="https://my.olin.edu/ics" class="" target="_blank">My.Olin.edu</a>
              </li>

              <li>
                <a href="http://olin.edu/about/" class="" target="_blank">About Olin</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--footer ends here-->
</div><!-- End of Wrapper -->

  <!--scripts starts here-->
  <script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/general.js?<?php echo filemtime(get_template_directory() . '/js/general.js'); ?>"></script>
  <!--scripts ends here-->
</body>

</html>
