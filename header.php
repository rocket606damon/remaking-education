<?php header('Content-Type: text/html; charset=utf-8'); ?>
<?php if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
        header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<?php $displayTitle = isset($titleOverride) ? $titleOverride : get_the_title(); ?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title>Olin College of Engineering | <?php echo $displayTitle; ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  	<meta name="format-detection" content="telephone=no">
  	<meta name="keywords" content="">
  	<meta name="description" content="">

		<!--Load fonts-->
		<script type="text/javascript">
      WebFontConfig = {
        google: { 
          families: [
            'Quicksand:300,400,500,700',
            'Lato:400,400i,700,700i',
            'Oxygen:400,700',
            'Pathway+Gothic+One'
          ]
        },
        timeout: 2000
      };
      (function() {
       var wf = document.createElement('script');
       wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
           '://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
       wf.type = 'text/javascript';
       wf.async = 'true';
       var s = document.getElementsByTagName('script')[0];
       s.parentNode.insertBefore(wf, s);
     })();
    </script>
    <!-- End of Load Fonts-->
    
    <!-- Stylesheets-->
    <link href="https://d2poexpdc5y9vj.cloudfront.net/public/css/eventzilla-widget-button.css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/modal.css?<?php echo filemtime(get_template_directory() . '/css/modal.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css<?php echo '?' . filemtime(get_stylesheet_directory() . '/style.css'); ?>">
  	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css?<?php echo filemtime(get_template_directory() . '/css/responsive.css'); ?>" media="screen">
    <!-- End of Stylesheets -->

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<div class="first-fold">
			  <!--header section starts-->
			  <header>
			    <div class="container">
			      <div class="row">
			        <div class="col-sm-12 header-main">
			          <div class="logo">
			            <a href="<?php the_field('olin_link', 'option'); ?>" target="_blank"><img src="<?php the_field('olin_image', 'option'); ?>" alt=""></a>
			            <a href="<?php the_field('emerson_link', 'option'); ?>" target="_blank"><img src="<?php the_field('emerson_image', 'option'); ?>" alt=""></a>
			          </div>
			          <div class="menu-box"><span></span><span></span><span></span></div>
			          <div class="nav">
			            <ul>
			              <li><a href="#schedule" title="schedule">schedule</a></li>
			              <li><a href="#speakers" title="speakers">speakers</a></li>
			              <li><a href="#supporters" title="supporters">supporters</a></li>
			              <li><a href="#producers" title="producers">producers</a></li>
			              <li><a href="#about" title="about">about</a></li>
			              <li><a href="#travel" title="travel">travel</a></li>
			              <li><a href="<?php the_field('register_link', 'option'); ?>" title="Register" target="_blank">Register</a></li>
			            </ul>
			          </div>
			        </div>
			      </div>
			    </div>
			  </header>
			  <div class="banner">
			  	<?php the_field('banner_title', 'option'); ?>
			  	<?php the_field('banner_caption', 'option'); ?>
			  </div>
			</div>
			<!--header section ends-->
