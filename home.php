<?php /* Template Name: Home */ ?>
<?php require_once('header.php'); ?>
<?php while ( have_posts() ) : the_post(); ?>
<!-- midd section starts here-->
<div id="mid-container">

  <div class="banner-img">
    <img src="<?php bloginfo('template_directory'); ?>/images/home-img.jpg" alt="">
  </div>
  <div class="home-content">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 offset-xl-1 col-lg-4 col-md-6">
          <ul>
          	<?php $i = 0; while(have_rows('midd_content')) : the_row(); ?>
            <li>
              <h3><?php the_sub_field('header'); ?></h3>
              <span><?php the_sub_field('subheading'); ?></span>
              <p><?php the_sub_field('description'); ?></p>
            </li>
            <?php $i++; endwhile; ?>
          </ul>
        </div>
        <div class="col-xl-5 offset-lg-1 col-lg-7 col-md-6">
          <?php the_field('midd_content_right'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="schedule-sec scroll-sec" id="schedule">
    <div class="sec-title">
      <img src="<?php bloginfo('template_directory'); ?>/images/schedule-icon.png" width="66" alt="">
      <h4>Schedule</h4>
    </div>
    <div class="schedule-bg">
      <div class="container">
        <div class="row">
          <div class="col-xl-5 offset-xl-1 col-lg-6 col-md-12">
            <ul>
            	<?php if(have_rows('schedule')): $k = 0; 
                while(have_rows('schedule')) : $k++; the_row(); 

                $ow = get_field('schedule');
                $ouch = count($ow);

              ?>
              <li class="schedule-item-<?php echo $k; ?>">
                <figure>
                  <span><?php the_sub_field('time'); ?></span>
                  <?php the_sub_field('headline'); ?>
                  <?php if($k !== $ouch): ?><em></em><?php endif; ?>
                </figure>
                <div class="popup" <?php if($k == $ouch){"style='display: none;'";} ?> >
                  <?php the_sub_field('popup_content', false, false); ?>
                  <a href="#" class="pop-arrow"></a>
                  <button type="button" class="close pop-close" aria-hidden="true">×</button>
                </div>
              </li>
              <?php endwhile; endif; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <a id="btnpreview" class="ezilla-widget-button ezilla-violet ewb-large btn" href="<?php the_field('register_link', 'option'); ?>" target="_blank">Register Now</a>
  </div>
  <div class="speakers-sec scroll-sec" id="speakers">
    <div class="sec-title">
      <img src="<?php bloginfo('template_directory'); ?>/images/speakers-icon.png" width="44" alt="">
      <h4>Speakers</h4>
      <p>Meet the Storytellers</p>
    </div>
    <div class="container-fluid">
      <div class="row">
        <ul class="col-sm-12">
          <?php if(have_rows('speakers')): $i = 0;
          while (have_rows('speakers')) : $i++; the_row();
          
          ?>

          <?php
              $modal = get_sub_field('name');
              $modal = preg_replace("/[^A-Za-z0-9 ]/", '', $modal);
              $modal = str_replace(" ", "-", $modal);
              $modal = strtolower($modal);

              $photo = get_sub_field('photo');

              //echo $modal;
          ?>
          <!--speaker-->
          <li>
            <a class="link" href="#" data-toggle="modal" data-target="#<?php echo $modal; ?>" style="background-image: url(<?php echo $photo; ?>);">
              <div class="speakers-hover">
                <p><?php the_sub_field('descriptor'); ?></p>
                <div class="speakers-details">
                  <u><?php the_sub_field('name'); ?></u>
                </div>
              </div>
            </a>
          </li>
          <!--modal-->
          <div class="modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $modal; ?>" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><?php the_sub_field('name'); ?> | <?php the_sub_field('title'); ?></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                  <div class="thumbnail left-align">
                    <img src="<?php echo $photo; ?>" alt="speaker-photo" style="display: block;">
                  </div>
                  <?php the_sub_field('content'); ?>
                </div>
              </div>
            </div>
          </div>
          <!--speaker + modal end-->
          <?php endwhile; endif; ?>
        </ul>

        <a id="btnpreview" class="ezilla-widget-button ezilla-violet ewb-large btn" href="<?php the_field('register_link', 'option'); ?>" target="_blank">Register Now</a>
      </div>
    </div>
  </div>
  <div class="action-sec">
    <?php the_field('action_sec_header'); ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 actionImg-cont">
          <img src="<?php the_field('action_sec_image1'); ?>" alt="">
        </div>
        <div class="col-md-6 actionImg-cont">
          <img src="<?php the_field('action_sec_image2'); ?>" alt="">
        </div>
      </div>
    </div>
  </div>
  <div class="supporter-sec scroll-sec" id="supporters">
    <div class="sec-title">
      <img src="<?php bloginfo('template_directory'); ?>/images/supporters-icon.png" width="66" alt="">
      <h4>supporters</h4>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="supporters-box">
            <?php $e = 0; while (have_rows('sponsor_type')) : $e++; the_row();?>
            <h5><?php the_sub_field('sponsor_type'); ?></h5>
            <?php if (get_sub_field('sponsor_description')): ?><?php the_sub_field('sponsor_description'); ?><?php endif; ?>
            <br>
            <?php if(have_rows('sponsors')) : $f = 0; 
              while(have_rows('sponsors')) : $f++; the_row(); 

              $rows = get_field('sponsors');
              $row_count = count($rows);

            ?>
              <a href="<?php the_sub_field('sponsor_url'); ?>" target="_blank" title=""><img src="<?php the_sub_field('sponsor_image'); ?>" alt=""></a>
              <?php if($f !== $row_count): ?><div class="clearfix"></div><?php endif; ?>
            <?php endwhile; endif; endwhile; ?> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="register-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <a id="btnpreview" class="ezilla-widget-button ezilla-violet ewb-large btn" href="<?php the_field('register_link', 'option'); ?>" target="_blank">Register Now</a>
        </div>
      </div>
    </div>
  </div>
  <div class="producers-sec scroll-sec" id="producers">
    <div class="sec-title">
      <img src=" <?php bloginfo('template_directory'); ?>/images/producers-icon.png" width="66" alt="">
      <h4>Remaking Education<br> producers</h4>
    </div>
    <div class="tab-main">
      <ul>
        <li class="tab-links active" title="dissenters" data-target="dissenters">dissenters</li>
        <li class="tab-links" title="deepeners" data-target="deepeners">deepeners</li>
        <li class="tab-links" title="Designers" data-target="designers">Designers</li>
        <li class="tab-links" title="Darers" data-target="darers">Darers</li>
        <li class="tab-links" data-target="doers" title="doers">doers</li>
      </ul>
      <?php $g = 0; while (have_rows('producer_type')) : $g++; the_row(); ?>
      <div data-id="<?php the_sub_field('type'); ?>" class="tab-content<?php if($g == 1){echo ' active'; } ?>">
        <div class="container">
          <div class="row">
            <div class="col-md-12 tab-imgs">

              <?php $h = 0; while (have_rows('producer_list')) : the_row(); ?>
              <figure style="background-image: url(<?php the_sub_field('image'); ?>)">
                <a class="link" href="#" data-toggle="modal" data-target="#modalOhWowOhBoi">
                  <?php the_sub_field('content', false, false); ?>
                </a>
              </figure>
              <?php $h++; endwhile; ?>

            </div>
          </div>
        </div>
      </div>
      <?php endwhile; ?>

  <div class="about-sec scroll-sec" id="about">
    <div class="sec-title">
      <img src="<?php bloginfo('template_directory'); ?>/images/about-icon.png" width="66" alt="">
      <h4>About</h4>
    </div>
    <div class="about-main">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="about-cont">
              <?php the_field('remaking_education_description'); ?>
              <a id="btnpreview" class="ezilla-widget-button ezilla-violet ewb-large btn" href="<?php the_field('register_link', 'option'); ?>" target="_blank">Register Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="location-sec scroll-sec" id="travel">
    <div class="sec-title">
      <img src="<?php bloginfo('template_directory'); ?>/images/boxes.png" width="66" alt="">
    </div>
    <?php the_field('loc_title'); ?>
    <div class="location-main">
      <div class="container">
        <div class="row">
          <div class="location-box">
            <?php the_field('loc_box'); ?>
          </div>
        </div>
      </div>
      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2948.4737577725764!2d-71.0648849845439!3d42.353741779187274!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e3709d4f8de005%3A0x2cc97c7028162964!2sParamount+Center!5e0!3m2!1sen!2sin!4v1526534003116"></iframe>
      </div>
      <!-- <div class="modal fade" id="modalForParking" tabindex="-1" role="dialog" aria-labelledby="modalForNameTwoTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <div class=container>
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12">
                    <iframe src="https://emersonparamount.org/Online/default.asp?doWork::WScontent::loadArticle=Load&BOparam::WScontent::loadArticle::article_id=CA295907-94E0-46E1-A4D1-BFBDD6403191&menu_id=45D3B732-773E-4EFE-8CAD-14E5D1287113&sToken=1%2Ca2ed643e%2C5b0d7118%2CF191B393-BC1F-48C2-826A-C851A04D1941%2CbQtNKlkVKCAwd6I3B5Ps8Bq8MpU%3D"></iframe>
                  </div>
                </div>
              </div>
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://artsemerson.org/Online/default.asp?doWork::WScontent::loadArticle=Load&BOparam::WScontent::loadArticle::article_id=8258B112-2B87-474C-811C-8CE00B4EFF84"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>
<!-- mid section ends here-->
<?php endwhile; ?>
<?php require_once('footer.php'); ?>