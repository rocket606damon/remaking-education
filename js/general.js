// JavaScript Document
var $ = jQuery.noConflict();

jQuery(document).ready(function(){
    $(".schedule-bg li em").click(function(){
        $(this).parent().parent("li").siblings("li").find(".popup").removeClass("show");
        $(this).parent().parent("li").siblings("li").removeClass("active");
        $(this).parent().siblings(".popup").toggleClass("show");
        $(this).parent().parent("li").toggleClass("active");
        $(".popup .pop-close, .popup .pop-arrow").click(function(e){
            e.preventDefault();
            $(".schedule-bg li.active em").trigger("click");   
        });
    });
        
    $(".tab-links").click(function(){
        var tabID = $(this).attr("data-target");
        $(".tab-content").removeClass("active");
        $(".tab-content[data-id='"+tabID+"']").addClass("active");
        $(".tab-links").removeClass("active");
        $(this).addClass("active");
    });
    $(".menu-box").click(function(){
        $(".nav").slideToggle();
        $(this).toggleClass("active");
    });
    /*$(document).on("click", "header a[href*='#']", function() {
        var headerHeight = $("header").outerHeight();
        $('html,body').animate({
            scrollTop: $(this.hash).offset().top - headerHeight
        }, 1000);
        $(".nav li").removeClass("active");
        $(this).parent("li").addClass("active");
        if($(".menu-box").is(":visible")) {
            $(".menu-box").trigger("click");
        }
        return false;
    });*/
     $(".nav li a[href*='#']").click(function (e) {
        e.preventDefault();
        var targt = $(this).attr("href")
        console.log(targt)
        $("html,body").stop().animate({
            scrollTop: $(targt).offset().top - $("header").outerHeight()
        }, 700)
        if($(".menu-box").is(":visible")) {
            $(".menu-box").trigger("click");
        }
    })
    activeNav()
    
    var anchors = document.getElementsByTagName('.speakers-sec li a');

document.body.addEventListener('click', function (e) {
    [].forEach.call(anchors, function (anchor) {
        if (e.target !== anchor) {
            anchor.classList.remove('highlight');
            anchor.classList.remove('follow');
        }
    });
}, false);

[].forEach.call(anchors, function (anchor) {
    anchor.addEventListener('click', (function (e) {
        var clicked = false;

        return function (e) {
            if (!clicked) {
                e.preventDefault();
                e.target.classList.add('highlight');
                clicked = true;
            } else {
                clicked = e.target.classList.contains('highlight');
                if (clicked) {
                    e.target.classList.add('follow');
                    e.target.classList.remove('highlight');
                } else {
                    e.preventDefault();
                    e.target.classList.add('highlight')
                    clicked = true;
                }
            }
        };
    }()), false);
});
});
$(window).resize(function(){
    menuHeight();
});
$(window).load(function(){
    stickyHeader();
    menuHeight();
});
$(window).scroll(function(){
    stickyHeader()
    activeNav()
});

function activeNav() {
    var scroll = $(window).scrollTop()
    $(".scroll-sec").each(function (i) {
        if (scroll > ($(this).offset().top - $("header").outerHeight() - 1) && scroll <= ($(this).offset().top + $(this).outerHeight())) {
            $(".scroll-sec").removeClass("active")
            $(this).addClass("active")
            $(".nav li").removeClass("active")
            $(".nav li").eq(i).addClass("active")
        } else {
            $(this).removeClass("active")
            $(".nav li").eq(i).removeClass("active")
        }
    })
}

function menuHeight() {
    if($(".menu-box").is(":visible")) {
        var headerHeight = $("header").outerHeight();
        var winHeight = $(window).innerHeight();
        var orignalHeight = winHeight - headerHeight;
        $(".nav").css("max-height", orignalHeight)
    } else {
        $(".nav").attr("style", "");
    }
}
function stickyHeader() {
    if($(window).scrollTop() > 0){
       $("header").addClass("stickyHeader");
    } else {
       $("header").removeClass("stickyHeader");
    }
}









